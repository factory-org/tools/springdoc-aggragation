package factory.tools.springdocaggregation.common

import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.SpecVersion
import io.swagger.v3.oas.models.info.Info
import org.slf4j.LoggerFactory

public object OpenApiMerger {
    public fun aggregateOpenApiSpecs(inputs: Collection<OpenAPI>, info: Info): OpenAPI {
        val outputSpec = OpenAPI(SpecVersion.V31)
        outputSpec.info(info)

        for (inputSpec in inputs) {
            for ((name, path) in inputSpec.paths) {
                val newName = "/api$name"
                outputSpec.path(newName, path)
            }
            if (inputSpec.components?.schemas != null) {
                for ((name, schema) in inputSpec.components.schemas) {
                    if (outputSpec.components?.schemas != null && outputSpec.components.schemas.containsKey(name)) {
                        if (schema != outputSpec.components.schemas[name]) {
                            logger.warn("Duplicate non matching schema \"{}\"", name)
                            continue
                        } else {
                            logger.debug("Found the same schema \"{}\" twice", name)
                        }
                    }

                    outputSpec.schema(name, schema)
                }
            }
        }

        return outputSpec
    }

    private val logger = LoggerFactory.getLogger(OpenApiMerger::class.java)
}
