plugins {
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.kotlin.spring)
    alias(libs.plugins.spring.dependencyManagement)
    `maven-publish`
    `java-library`
}

dependencies {
    implementation(libs.swagger.core)
    implementation(libs.logging.slf4j)
}

kotlin {
    jvmToolchain(11)
}

java {
    withSourcesJar()
}

publishing {
    publications {
        create<MavenPublication>("library") {
            from(components["java"])
            pom {
                licenses {
                    license {
                        name.set("The Apache License, Version 2.0")
                        url.set("https://www.apache.org/licenses/LICENSE-2.0.txt")
                    }
                }
                developers {
                    developer {
                        id.set("cromefire_")
                        name.set("Cromefire_")
                        email.set("cromefire+factory@pm.me")
                    }
                }
            }
        }
    }
    repositories {
        maven("https://gitlab.com/api/v4/projects/51237738/packages/maven") {
            name = "GitLab"

            credentials(HttpHeaderCredentials::class.java) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN") ?: ""
            }
            authentication {
                register("header", HttpHeaderAuthentication::class.java)
            }
        }
    }
}

