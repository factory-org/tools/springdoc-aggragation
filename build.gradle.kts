import org.jetbrains.kotlin.gradle.dsl.KotlinJvmProjectExtension

plugins {
    alias(libs.plugins.kotlin.jvm) apply false
}

val tag: String? = System.getenv("CI_COMMIT_TAG")
val snapshotVersion = "0.0.3-SNAPSHOT"

group = "factory.tools.springdocaggregation"
version = if (!tag.isNullOrBlank() && tag.startsWith("v")) tag.substring(1) else snapshotVersion

subprojects {
    afterEvaluate {
        group = rootProject.group
        version = rootProject.version

        configurations {
            named("compileClasspath") {
                resolutionStrategy.activateDependencyLocking()
            }
            named("runtimeClasspath") {
                resolutionStrategy.activateDependencyLocking()
            }
        }

        extensions.getByType<KotlinJvmProjectExtension>().apply {
            explicitApi()
        }

        repositories {
            mavenCentral()
        }

        tasks.withType<Test> {
            useJUnitPlatform()
        }
    }
}
