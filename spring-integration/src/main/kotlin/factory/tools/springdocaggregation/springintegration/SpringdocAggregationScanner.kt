package factory.tools.springdocaggregation.springintegration

import org.springframework.context.annotation.ComponentScan
import org.springframework.stereotype.Component

@Component
@ComponentScan("de.megla.contrib.seatreserve.utils.springdocaggregation")
public class SpringdocAggregationScanner
