package factory.tools.springdocaggregation.springintegration

import org.springframework.context.annotation.Import


@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(SpringdocAggregationScanner::class)
public annotation class EnableSpringdocAggregation
