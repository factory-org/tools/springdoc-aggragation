package factory.tools.springdocaggregation.springintegration

import jakarta.validation.constraints.NotNull
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component
import org.springframework.validation.annotation.Validated
import java.net.URI


@Component
@ConfigurationProperties("springdoc.aggregation")
public class SpringdocAggregationConfiguration {
    public var targets: List<AggregationTarget> = listOf()

    @Validated
    public class AggregationTarget {
        @NotNull
        public lateinit var uri: URI
    }
}
