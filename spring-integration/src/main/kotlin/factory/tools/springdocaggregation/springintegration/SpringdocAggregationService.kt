package factory.tools.springdocaggregation.springintegration

import com.fasterxml.jackson.databind.JsonNode
import factory.tools.springdocaggregation.common.OpenApiMerger
import io.swagger.v3.core.util.OpenAPI30To31
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.parser.OpenAPIV3Parser
import kotlinx.coroutines.*
import kotlinx.coroutines.reactor.*
import org.slf4j.LoggerFactory
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.bodyToMono

@Service("springdocAggregationService")
public class SpringdocAggregationService(
    private val configuration: SpringdocAggregationConfiguration,
    clientBuilder: WebClient.Builder,
) {
    private val scope = CoroutineScope(Dispatchers.IO)

    private val client: WebClient = clientBuilder.build()

    public suspend fun buildOpenApiSpec(): OpenAPI {
        val deferredResults =
            mutableListOf<Deferred<OpenAPI>>()
        val parser = OpenAPIV3Parser()
        val converter = OpenAPI30To31()
        logger.debug("Fetching child OpenAPI specs...")
        for (target in configuration.targets) {
            val res = scope.async {
                val response = client.get()
                    .uri(target.uri)
                    .accept(MediaType.APPLICATION_JSON)
                    .retrieve()
                    .bodyToMono<JsonNode>()
                    .map {
                        val res = parser.parseJsonNode(target.uri.toString(), it)
                        val openAPI: OpenAPI = res.openAPI
                        if (!res.isOpenapi31) {
                            converter.process(openAPI)
                        }
                        openAPI
                    }
                    .awaitSingle()
                response
            }
            deferredResults.add(res)
        }
        val results = deferredResults.awaitAll()
        val info = Info()
        info.title = "Aggregated Schema"

        val aggregatedSpec = OpenApiMerger.aggregateOpenApiSpecs(results, info)
        logger.debug("Built aggregated OpenAPI spec")
        return aggregatedSpec
    }

    private companion object {
        private val logger = LoggerFactory.getLogger(SpringdocAggregationService::class.java)
    }
}
