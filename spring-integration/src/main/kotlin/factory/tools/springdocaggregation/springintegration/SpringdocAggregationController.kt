package factory.tools.springdocaggregation.springintegration

import io.swagger.v3.core.util.Json
import org.springframework.http.HttpHeaders
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
public class SpringdocAggregationController(
    private val aggregationService: SpringdocAggregationService,
) {
    @GetMapping("v3/aggregated-api-docs")
    public suspend fun aggregatedApiDocs(): ResponseEntity<String> {
        val spec = aggregationService.buildOpenApiSpec()
        val json = Json.mapper().writeValueAsString(spec)

        val headers = HttpHeaders().apply {
            add("Content-Type", "application/json")
        }
        return ResponseEntity.ok()
            .headers(headers)
            .body(json)
    }
}
