plugins {
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.kotlin.spring)
    alias(libs.plugins.spring.dependencyManagement)
    `maven-publish`
    `java-library`
}

dependencies {
    implementation(project(":common"))

    implementation(libs.springdoc.common)
    implementation(libs.spring.framework.webflux)
    implementation(libs.swagger.parser)
    implementation(libs.kotlinx.coroutines)
    implementation(libs.kotlinx.coroutines.reactor)
}

kotlin {
    jvmToolchain(17)
    compilerOptions {
        freeCompilerArgs.add("-Xjsr305=strict")
    }
}

java {
    withSourcesJar()
}

publishing {
    publications {
        create<MavenPublication>("library") {
            from(components["java"])
            pom {
                licenses {
                    license {
                        name.set("The Apache License, Version 2.0")
                        url.set("https://www.apache.org/licenses/LICENSE-2.0.txt")
                    }
                }
                developers {
                    developer {
                        id.set("cromefire_")
                        name.set("Cromefire_")
                        email.set("cromefire+factory@pm.me")
                    }
                }
            }
        }
    }
    repositories {
        maven("https://gitlab.com/api/v4/projects/51237738/packages/maven") {
            name = "GitLab"

            credentials(HttpHeaderCredentials::class.java) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN") ?: ""
            }
            authentication {
                register("header", HttpHeaderAuthentication::class.java)
            }
        }
    }
}
