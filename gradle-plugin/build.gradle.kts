plugins {
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.kotlin.allOpen)
    `java-gradle-plugin`
    alias(libs.plugins.gradle.pluginPublish)
    `maven-publish`
}

repositories {
    gradlePluginPortal()
}

dependencies {
    implementation(project(":common"))

    implementation(libs.swagger.parser)
    implementation(libs.springdoc.plugin)
}

configurations {
    configureEach {
        attributes {
            attribute(TargetJvmVersion.TARGET_JVM_VERSION_ATTRIBUTE, 11)
        }
    }
}

allOpen {
    annotation("org.gradle.api.tasks.CacheableTask")
}

kotlin {
    jvmToolchain(11)
}

java {
    withSourcesJar()
}

@Suppress("UnstableApiUsage")
gradlePlugin {
    website.set("https://gitlab.com/factory-org/tools/springdoc-aggragation")
    vcsUrl.set("https://gitlab.com/factory-org/tools/springdoc-aggragation")
    plugins {
        create("springdocaggregation") {
            id = "factory.tools.springdocaggregation"
            displayName = "Springdoc Aggregation Plugin"
            description = "A plugin that can aggregate"
            implementationClass = "factory.tools.springdocaggregation.gradleplugin.SpringAggregationPlugin"
            tags.set(listOf("spring", "springdoc"))
        }
    }
}

publishing {
    publications {
        withType<MavenPublication> {
            pom {
                name.set("springdocaggregation-gradle-plugin")
                licenses {
                    license {
                        name.set("The Apache License, Version 2.0")
                        url.set("https://www.apache.org/licenses/LICENSE-2.0.txt")
                    }
                }
                developers {
                    developer {
                        id.set("cromefire_")
                        name.set("Cromefire_")
                        email.set("cromefire+factory@pm.me")
                    }
                }
            }
        }
    }
    repositories {
        maven("https://gitlab.com/api/v4/projects/51237738/packages/maven") {
            name = "GitLab"

            credentials(HttpHeaderCredentials::class.java) {
                name = "Job-Token"
                value = System.getenv("CI_JOB_TOKEN") ?: ""
            }
            authentication {
                register("header", HttpHeaderAuthentication::class.java)
            }
        }
    }
}
