package factory.tools.springdocaggregation.gradleplugin

import org.gradle.api.artifacts.Configuration
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.CacheableTask
import org.gradle.api.tasks.Internal
import java.io.File

@CacheableTask
public abstract class AggregateFromConfigurationTask : BaseAggregateTask() {
    @get:Internal
    public final val sourceConfiguration: Property<Configuration> =
        project.objects.property(Configuration::class.java)

    init {
        dependsOn.add(sourceConfiguration)
    }

    override val inputSpecFiles: Provider<Collection<File>>
        get() = sourceConfiguration.map { config ->
            config.files
        }
}
