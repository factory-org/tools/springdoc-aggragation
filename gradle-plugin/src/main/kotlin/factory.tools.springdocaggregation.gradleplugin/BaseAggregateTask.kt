package factory.tools.springdocaggregation.gradleplugin

import factory.tools.springdocaggregation.common.OpenApiMerger
import io.swagger.v3.core.util.Json
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.parser.OpenAPIV3Parser
import org.gradle.api.DefaultTask
import org.gradle.api.file.RegularFileProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.tasks.*
import java.io.File
import kotlin.io.path.writeText

public abstract class BaseAggregateTask : DefaultTask() {
    @get:InputFiles
    @get:PathSensitive(PathSensitivity.NONE)
    protected abstract val inputSpecFiles: Provider<Collection<File>>
    private val jsonMapper = Json.pretty()

    @get:Internal
    public val info: Property<Info> = project.objects.property(Info::class.java)
        .convention(
            Info().also {
                it.title = project.name
                it.version = project.version.toString()
            },
        )

    @get:Input
    public val infoString: Provider<String>
        get() = info.map { jsonMapper.writeValueAsString(it) }

    @get:OutputFile
    public abstract val outputFile: RegularFileProperty

    @TaskAction
    public fun exec() {
        val inputFiles = inputSpecFiles.get()
        val info = info.get()
        val outputFile = outputFile.get().asFile.toPath()

        val parser = OpenAPIV3Parser()
        val inputSpecs = mutableListOf<OpenAPI>()
        for (inFile in inputFiles) {
            val inText = inFile.readText()
            val response = parser.readContents(inText, null, null)
            inputSpecs.add(response.openAPI)
        }

        val spec = OpenApiMerger.aggregateOpenApiSpecs(inputSpecs, info)

        val json = jsonMapper.writeValueAsString(spec)
        outputFile.writeText(json)
    }
}
