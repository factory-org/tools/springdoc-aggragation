package factory.tools.springdocaggregation.gradleplugin

import org.gradle.api.file.RegularFile
import org.gradle.api.provider.Provider
import org.gradle.api.provider.SetProperty
import org.gradle.api.tasks.CacheableTask
import org.gradle.api.tasks.Internal
import java.io.File

@CacheableTask
public abstract class AggregateFromFilesTask : BaseAggregateTask() {
    @get:Internal
    public abstract val inputFiles: SetProperty<RegularFile>

    override val inputSpecFiles: Provider<Collection<File>>
        get() = inputFiles.map { it.map(RegularFile::getAsFile) }
}
