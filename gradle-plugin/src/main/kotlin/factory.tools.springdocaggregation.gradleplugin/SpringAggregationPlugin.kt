package factory.tools.springdocaggregation.gradleplugin

import org.gradle.api.Plugin
import org.gradle.api.Project

public class SpringAggregationPlugin : Plugin<Project> {
    @Suppress("UnstableApiUsage")
    override fun apply(target: Project) {
        val serviceSpec = target.configurations.register("serviceSpec") {
            it.isCanBeResolved = true
            it.isCanBeDeclared = true
            it.isCanBeConsumed = false
        }

        target.tasks.register("aggregateServiceSpecs", AggregateFromConfigurationTask::class.java) { task ->
            task.dependsOn(serviceSpec)
            task.group = "openapi"
            task.sourceConfiguration.set(serviceSpec)
            task.outputFile.set(target.layout.buildDirectory.file("aggregated-openapi.yaml"))
        }

        // Task to dsl
        val extraProperties = target.extensions.extraProperties
        extraProperties[AggregateFromFilesTask::class.java.simpleName] = AggregateFromFilesTask::class.java
        extraProperties[AggregateFromConfigurationTask::class.java.simpleName] =
            AggregateFromConfigurationTask::class.java
    }
}
